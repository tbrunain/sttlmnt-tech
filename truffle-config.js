var HDWalletProvider = require("truffle-hdwallet-provider");
var PrivateKeyProvider = require("truffle-privatekey-provider");

const path = require("path");

module.exports = {
    compilers: {
        solc: {
            version: "0.5.8",
        }
    },
    networks: {
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*" // Match any network id
        },
        test: {
            host: "127.0.0.1",
            port: 8545,
            network_id: "*" // Match any network id
        },
        goerli: {
            provider: () => new HDWalletProvider(, 'https://goerli.infura.io/v3/'),
            network_id: '5', // eslint-disable-line camelcase
            gas: 4465030,
            gasPrice: 10000000000,
        },
    },
    contracts_build_directory: path.join(__dirname, "app/src/contracts"),

};
