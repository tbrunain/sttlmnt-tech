import React, {  Component } from "react";
import { DrizzleProvider } from "drizzle-react";
import { LoadingContainer } from "drizzle-react-components";

import "./App.css";

import drizzleOptions from "./drizzleOptions";
import MyContainer from "./MyContainer";
import {Drizzle, generateStore} from "drizzle";

// 2. Setup the drizzle instance.
const drizzleStore = generateStore(drizzleOptions);
const drizzle = new Drizzle(drizzleOptions, drizzleStore);

class App extends Component {
  render() {
    return (
      <DrizzleProvider options={drizzleOptions}>
        <LoadingContainer>
          <MyContainer drizzle={drizzle}/>
        </LoadingContainer>
      </DrizzleProvider>
    );
  }
}

export default App;
