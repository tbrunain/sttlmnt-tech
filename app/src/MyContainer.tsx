import MyComponent from "./MyComponent";
import { drizzleConnect } from "drizzle-react";

const mapStateToProps = (state, drizzle) => {
  console.log("DRIZZLE ", drizzle);
  return {
    accounts: state.accounts,
    DesertFestTicket: state.contracts.DesertFestTicket,
    Tickets: state.contracts.DesertFestTicket.getTokensId,
    drizzleStatus: state.drizzleStatus,
    drizzleContext: state.drizzleContext,
    drizzle: drizzle,
  };
};


const MyContainer = drizzleConnect(MyComponent, mapStateToProps);

export default MyContainer;
