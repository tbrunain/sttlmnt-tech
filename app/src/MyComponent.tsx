import React, {Component} from "react";
import {AccountData, ContractData, ContractForm} from "drizzle-react-components";
import Table from "@material-ui/core/Table";
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import logo from "./logo.png";

type Props = {
    drizzle: any,
    accounts: any,
    DesertFestTicket: any
};

interface State {
    price: number
};

interface SellableTicketInfo {
    isSellable: boolean,
    price: number,
    tokenId: number,
    owner: number
}


// export default ({accounts, drizzle, DesertFestTicket, Tickets, drizzleContext}) => (
class MyComponent extends Component<Props, State> {

    state: State = {
        price: 100000000000000,
    };

    componentDidMount() {
        const {drizzle, accounts} = this.props;
    }

    updatePrice(e) {
        this.setState({['price']: e.target.value});
    }

    render() {
        const {drizzle, accounts, DesertFestTicket} = this.props;

        let sellableTokens: SellableTicketInfo[] = [];

        sellableTokens.map(value => console.log("Onion ", value));
        return (
            <div className="App">
                <div>
                    <img src={logo} alt="drizzle-logo"/>
                    <h1>Tech Int. Demo</h1>
                    <p>Quick and really dirty dapp about selling ticket (ERC721 token) for a festival .</p>
                </div>

                <div className="section">
                    <h2>Active Account</h2>
                    <AccountData accountIndex="0" units="ether" precision="3"/>
                </div>

                <div className="section">
                    <h2>DesertFestTicket</h2>
                    <p>
                        Desertfest Belgium is one of several annual festivals which run under the DesertFest banner.
                        Each
                        year, the festival is a three day event showcasing bands from the whole stoner rock scene.
                        The
                        festival takes place in Antwerp, Belgium.
                    </p>
                    <div>
                        <strong>Stored Value: </strong>
                        <p>
                            Symbol :
                            <ContractData contract="DesertFestTicket" method="symbol"/>
                        </p>
                        <p>
                            Is contract paused ? :
                            <ContractData contract="DesertFestTicket" method="paused"/>
                        </p>
                        <p>
                            Balance of the contract :
                            <ContractData contract="DesertFestTicket" method="balanceOfContract"/> wei
                        </p>
                        <p>
                            Name :
                            <ContractData contract="DesertFestTicket" method="name"/>
                        </p>
                        <p>
                            Total number of ticket existing :
                            <ContractData contract="DesertFestTicket" method="totalSupply"/>
                        </p>
                        <p>
                            Total number of ticket owned by current user :
                            <ContractData contract="DesertFestTicket" method="balanceOf"
                                          methodArgs={[accounts[0]]}/>
                        </p>
                        <p>
                            Token Id currently existing :

                            <ContractData contract="DesertFestTicket" method="getTokensId"/>

                            <br/>
                            More detailed info :
                            <Paper>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Token Id</TableCell>
                                            <TableCell align="right">Price in WEI</TableCell>
                                            <TableCell align="right">On sale ?</TableCell>
                                            <TableCell align="right">Currentowner</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {Object.keys(DesertFestTicket.getTokensId).map(a => {
                                            return Object.keys(DesertFestTicket.getTokensId[a]).map(properties => {
                                                if (properties === 'value') {
                                                    return DesertFestTicket.getTokensId[a][properties].map(item => {
                                                        return <tr>
                                                            <td>{item}</td>
                                                            <td><ContractData contract="DesertFestTicket"
                                                                              method="getTicketPrice"
                                                                              methodArgs={[item]}/></td>
                                                            <td><ContractData contract="DesertFestTicket"
                                                                              method="isTokenOnSale"
                                                                              methodArgs={[item]}/></td>
                                                            <td><ContractData contract="DesertFestTicket"
                                                                              method="ownerOf" methodArgs={[item]}/>
                                                            </td>
                                                        </tr>
                                                    })
                                                }
                                            })
                                        })}
                                    </TableBody>
                                </Table>
                            </Paper>
                        </p>
                    </div>
                    <div className="section">
                        <p>
                            Wanna buy a ticket from the organizer ?
                            <ContractForm contract="DesertFestTicket" method="mintTicket"
                                          sendArgs={{value: "1000000000000000"}}/>
                        </p>
                        <p>
                            Wanna buy a ticket from someone else ?

                            <input
                                type='number'
                                className='form-control'
                                pattern='[0-9]{0,5}'
                                onInput={(event) => this.updatePrice(event)}
                            />
                            <ContractForm contract="DesertFestTicket" method="buyToken"
                                          sendArgs={{value: this.state.price}}/>
                        </p>
                        <p>
                            Wanna sell a ticket ?
                            <ContractForm contract="DesertFestTicket" method="setTokenAsSellable"/>
                        </p>
                    </div>
                    <div className="section">
                        <h3>Only for the owner of the contract</h3>
                        <p>
                            Wanna withdraw what is stored in the contract ?
                            <ContractForm contract="DesertFestTicket" method="withdraw"/>
                        </p>
                        <p>
                            In case of emergency you can stop the contract here :
                            <ContractForm contract="DesertFestTicket" method="pause"/>
                        </p>
                        <p>
                            Or resume operations here :
                            <ContractForm contract="DesertFestTicket" method="unpause"/>
                        </p>
                    </div>
                </div>
            </div>)
    }
}

export default MyComponent;