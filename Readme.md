# Requirements

    Create an ERC721 token (http://erc721.org/) that represents tickets for a festival. 
        There are maximum 1000 tickets
        You can buy tickets from the organiser at 1 finney per ticket
        You can buy for and sell tickets to others, but the price can never be higher than 110% of the previous sale
        Optional: add a monetisation option for the organiser in the between user sales

    Create a quick and dirty dapp to show the current state (supply, what the accounts own, eth balances) and buttons to demonstrate the different functions. 

# Minimal requirements:

    Use Solidity, Truffle and React (bonus: Typescript)
    Deploy to the Görli test network and deploy the dapp somewhere publicly accessible (e.g now.sh or Heroku)
    Document your code so the reviewer can understand what you are doing

# Delivery:

    Send me a Github/GitLab/… link to the source code and the link to the running application
    
    
# Assumptions:

    I'll not use any sophisticated pattern like proxy / eternal storage . I want to have this dapp as simple as possible .
    
    I'll use ganache as local test env' .
    
    I'll use truffle box containing drizzle (see : https://truffleframework.com/boxes/drizzle)
    
    I'll use open-zeppelin ERC721 library .
    
    Since it's not really mentionned I assume that this token is for a specific festival .
    
    That festival starts on a certain date, we must block the action after that start date .
    
    
 ### Festival
 
    Maximum 1000 tickets.
    Initial price 1 Finney.
        - 0,001 Eth = 1 Finney.
    2 mains actors :
        - Organiser of festival.
        - Buyer of tickets.
            - They can resell tickets they bought, at maximum 110% of previous sale.
                Ex: Bought it for 0,001 Finney, he can resell it for maximum 0.0011 Finney.
 
    
    
     