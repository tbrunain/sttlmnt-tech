pragma solidity ^0.5.8;

import "openzeppelin-solidity/contracts/token/ERC721/ERC721Full.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Mintable.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721MetadataMintable.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Burnable.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";


/// @title A simple ERC721 token representing a ticket for a festival .
/// @author tbrunain
/// @notice This has not been tested extensively !!!! (By lack of time sadly)
contract DesertFestTicket is ERC721Full, ERC721Mintable, ERC721MetadataMintable, Ownable, ReentrancyGuard, Pausable {

    using SafeMath for uint256;

    struct TicketInfo {
        uint256 ticketPrice;
        bool isSellable;
        uint256 index;
    }

    /// uint representing the initial price of a ticket , 1 Finney
    uint internal constant INITIAL_PRICE = 1;
    /// string for the token URI
    string internal constant TOKEN_BASE_URI = "desertfesttoken";
    /// uint representing the max amount of ticket that can be creted
    uint  internal _maxTicket;
    /// uint representing an amount in WEI that will be paid to the organizer on each sale .
    uint internal _organizerCut;

    mapping(uint256 => TicketInfo) internal _ticketsInfo;
    uint256[] internal _tickets;

    constructor (
        string memory name,
        string memory symbol,
        uint maxTicket,
        uint organizerCut) public ERC721Mintable() ERC721Full(name, symbol) {

        require(maxTicket > 0 && maxTicket <= 1000,
            "Maximum amount of ticket should be comprise in between 0 and 1000");
        require(organizerCut <= 1000000000 wei && organizerCut >= 0 wei,
            "Organizer cut should be equal or lower than 1 Gwei and greater or equal to zero");
        _organizerCut = organizerCut;
        _maxTicket = maxTicket;
    }

    /// EXTERNAL/PUBLIC FUNCTIONS ///

    /// @notice Will create a new token and assign it to msg.sender if all conditions are met
    /// @dev The Alexandr N. Tetearing algorithm could increase precision
    /// @return if action succedded or not
    function mintTicket() external payable whenNotPaused returns (bool)  {
        uint256 msgValue = msg.value;
        require(totalSupply() < _maxTicket, "We reached the maximum amount of token .");
        require(msgValue.div(1 finney) == INITIAL_PRICE, "Initial price is 1 Finney");

        uint tokenId = totalSupply();
        _mint(msg.sender, tokenId);
        _setTokenURI(tokenId, TOKEN_BASE_URI);
        newTicketInfo(tokenId, 1 finney);
        return true;
    }

    /// @notice Will mark a token as sellable for a certain price by its owner
    /// @param tokenId id of the token we want to mark as sellable
    /// @param price amount in WEI for which we want to sell this token
    function setTokenAsSellable(uint tokenId, uint256 price) external whenNotPaused {
        require(super._exists(tokenId), "That tokenId does not exist");
        require(ownerOf(tokenId) == msg.sender, "msg.sender is not the owner of this token");
        require(_isPriceCorrect(price, tokenId), "New price should be in between previousPrice and previousPrice + 10%");
        super.approve(address(this), tokenId);
        updateTicketInfo(tokenId, true, price);
    }

    /// @notice Will return a boolean indicating if a token is currently marked as 'on sale' by its owner or not.
    /// @param tokenId id of the token for which we want to check if it's on sale or not .
    /// @return bool indicating if that token is on sale or not .
    function isTokenOnSale(uint256 tokenId) public view returns (bool) {
        require(isExisting(tokenId), "That token does not exist .");
        return _ticketsInfo[tokenId].isSellable;
    }

    /// @notice Will change the ownership of a token if all conditions are met and transfer eth to previous owner.
    /// @param tokenId id of the token that we want to buy
    function buyToken(uint tokenId) external nonReentrant whenNotPaused payable {
        uint256 msgValue = msg.value;

        require(super._exists(tokenId), "That tokenId does not exist");
        require(isTokenOnSale(tokenId), "That token is not marked as sellable");
        require(ownerOf(tokenId) != msg.sender, "Mate ... you own that token already !");
        require(msgValue == _ticketsInfo[tokenId].ticketPrice, "Check the price again mate .");

        updateTicketInfo(tokenId, false, msg.value);

        //We substract the organizer cut from the msg.value .
        uint256 toTokenOwner = msgValue.sub(_organizerCut);

        address buyer = msg.sender;
        address payable to = address(uint160(ownerOf(tokenId)));
        super._transferFrom(ownerOf(tokenId), buyer, tokenId);

        to.transfer(toTokenOwner);
    }

    /// @notice Will return an array of tokenId belonging to a specified address .
    /// @param owner address for which we want to retrieve a list of owned tokens
    function getAllTokenByAddress(address owner) external view returns (uint256[] memory) {
        return _tokensOfOwner(owner);
    }

    /// @notice Will return an array of all tokenIds existing .
    /// @return array of tokenId
    function getTokensId() external view returns (uint256[] memory) {
        return _tickets;
    }

    /// @notice The current price for a specified token .
    /// @dev Will fail if tokenId is not existing
    /// @param tokenId for which we want to retrieve the price in WEI
    /// @return price in WEI of this token
    function getTicketPrice(uint256 tokenId) external view returns (uint256) {
        require(isExisting(tokenId), "That token does not exist .");
        return _ticketsInfo[tokenId].ticketPrice;
    }

    /// @notice Return the balance of the contract .
    function balanceOfContract() external view returns (uint256) {
        return address(this).balance;
    }

    /// @notice Will initiate a transfer of fund held in the contract to the address of the owner .
    function withdraw() external onlyOwner whenNotPaused {
        address payable to = address(uint160(owner()));
        to.transfer(address(this).balance);
    }

    /// @notice overload of approve function to add pausable functionality
    function approve(address to, uint256 tokenId) public whenNotPaused {
        super.approve(to, tokenId);
    }

    /// @notice overload of setApprovalForAll function to add pausable functionality
    function setApprovalForAll(address to, bool approved) public whenNotPaused {
        super.setApprovalForAll(to, approved);
    }

    /// @notice overload of transferFrom function to add pausable functionality
    function transferFrom(address from, address to, uint256 tokenId) public whenNotPaused {
        super.transferFrom(from, to,tokenId);
    }

    /// @notice overload of safeTransferFrom function to add pausable functionality
    function safeTransferFrom(address from, address to, uint256 tokenId) public whenNotPaused {
        super.safeTransferFrom(from, to, tokenId);
    }

    /// @notice overload of safTransferFrom function to add pausable functionality
    function safeTransferFrom(address from, address to, uint256 tokenId, bytes memory _data) public whenNotPaused {
        super.safeTransferFrom(from, to, tokenId, _data);
    }

    /// @notice overload of mint function to add pausable functionality
    function mint(address to, uint256 tokenId) public onlyMinter whenNotPaused returns (bool) {
        return super.mint(to, tokenId);
    }




        /// INTERNAL FUNCTIONS

    /// @notice Check if provided tokenId is already existing in _tickets array .
    /// @param tokenId for which we want to check .
    /// @return boolean indicating whether the tokenId exist or not .
    function isExisting(uint256 tokenId) internal view returns (bool isIndeed) {
        if (_tickets.length == 0) return false;
        return (_tickets[_ticketsInfo[tokenId].index] == tokenId);
    }

    /// @notice Create a new entry in _ticketsInfo mapping and _tickets array representing a new ticket
    /// @param tokenId we want to add to the mapping/list .
    /// @return boolean indicating whether the action was performed or not .
    function newTicketInfo(uint256 tokenId, uint256 price) internal whenNotPaused returns (bool success) {
        require(!isExisting(tokenId), "This ticket exists !");
        _ticketsInfo[tokenId].isSellable = false;
        _ticketsInfo[tokenId].ticketPrice = price;
        _ticketsInfo[tokenId].index = _tickets.push(tokenId) - 1;
        return true;
    }

    /// @notice Upadate an entry in _ticketsInfo mapping
    /// @param tokenId we want to add to the mapping/list .
    /// @param isSellable updated data value.
    /// @param newPrice updated data value .
    /// @return boolean indicating whether the action was performed or not .
    function updateTicketInfo(uint256 tokenId, bool isSellable, uint256 newPrice) internal whenNotPaused returns (bool success) {
        require(isExisting(tokenId), "Cannot update info of this token , does not exists");
        _ticketsInfo[tokenId].isSellable = isSellable;
        _ticketsInfo[tokenId].ticketPrice = newPrice;
        return true;
    }

    /// @notice Check whether a price passed in parameter is compliant with the rule saying that a token can be sold to
    /// another person (ie not from the 'organizer') at maximum 110% of the precedent sale .
    /// @return a boolean indicating if the price is compliant
    function _isPriceCorrect(uint256 price, uint256 tokenId) internal returns (bool) {
        return price >= _ticketsInfo[tokenId].ticketPrice && price <= (_ticketsInfo[tokenId].ticketPrice + (_ticketsInfo[tokenId].ticketPrice * 10 / 100));
    }
}
